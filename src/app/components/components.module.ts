import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import { AlbumComponent } from './album/album.component';
import { PhotoComponent } from './photo/photo.component';
import { IonicModule } from '@ionic/angular';


const components = [
  AlbumComponent,
  PhotoComponent
];

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: components,
  exports: [...components]
})
export class ComponentsModule {
}