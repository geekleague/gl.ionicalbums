import { Component, OnInit, Input } from '@angular/core';
import { IAlbum } from '../../models/album-interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss'],
})
export class AlbumComponent implements OnInit {

  constructor(private router: Router) { }
  @Input() item: IAlbum;

  ngOnInit() {}
  redirectTo() {
    console.log('entro', this.item.id)
    this.router.navigate([`/album/${this.item.id}`])
  }
}
