import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPhoto } from '../../models/photo-interface';
import { SearchService } from 'src/app/service/search.service';
import { AlbumsService } from '../../service/albums.service';
import { IAlbum } from 'src/app/models/album-interface';

@Component({
  selector: 'app-album',
  templateUrl: './album.page.html',
  styleUrls: ['./album.page.scss'],
})
export class AlbumPage implements OnInit {
  public id: any
  public album: IAlbum = {
    title: ''
  }
  public datas: Array<IPhoto> = []
  constructor(private route: ActivatedRoute,
              private service: AlbumsService,
              private search: SearchService
            ) {
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id')
    this.getData()
  }
  getData() {
    this.service.getAlbumId(this.id).then((response: IAlbum) => {
      this.album = response
      console.log(this.album, 'this.album')
    })
    this.service.getPhotos(this.id).then((response: Array<IPhoto>) => {
      this.datas = response
    })
  }
  get photos() {
    if (this.search.text === '') {
      return this.datas;
    } else {
      return this.datas.filter(item => {
        return item.title.includes(this.search.text.toLowerCase())
      })
    }
  }

}
