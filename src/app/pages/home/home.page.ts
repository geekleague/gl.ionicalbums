import { Component, OnInit } from '@angular/core';

import { AlbumsService } from '../../service/albums.service';
import { IAlbum } from '../../models/album-interface';
import { SearchService } from '../../service/search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})


export class HomePage implements OnInit {
  public datas: Array<IAlbum> = []
  constructor(private service: AlbumsService, private search: SearchService) { }

  ngOnInit() {
    this.getData()
  }
  get albums() {
    if (this.search.text === '') {
      return this.datas;
    } else {
      return this.datas.filter(item => {
        return item.title.includes(this.search.text.toLowerCase())
      })
    }
  }
  getData() {
    this.service.getAlbum().then((response: Array<IAlbum>) => {
      this.datas = response
    })
  }

}
