import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(private http: HttpClient) { }

  getAlbum () {
    return new Promise(resolve => {
      this.http.get('https://jsonplaceholder.typicode.com/albums').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getAlbumId (id) {
    return new Promise(resolve => {
      this.http.get(`https://jsonplaceholder.typicode.com/albums?id=${id}`).subscribe(data => {
        resolve(data[0]);
      }, err => {
        console.log(err);
      });
    });
  }

  getPhotos (id) {
    return new Promise(resolve => {
      this.http.get(`https://jsonplaceholder.typicode.com/photos?albumId=${id}`).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
    
  }
}
