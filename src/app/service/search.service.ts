import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  public text: string = ''
  constructor() { }
  getText () {
    return this.text;
  }
  SetText (value:string) {
    this.text = value
  }
}
